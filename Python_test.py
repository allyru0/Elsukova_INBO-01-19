from main import measures


def test1():
    assert measures(100, "см", "м") == 1


def test2():
    assert measures(3, "км","м") == 3000


def test3():
    assert measures(1, "м","дм") == 10


def test4():
    assert measures(500, "мм","см") == 50


def test5():
    assert measures(500, "мм", "дм") == 5


def test6():
    assert measures(500, "мм", "м") == 0.5


def test7():
    assert measures(500, "мм", "км") == 0.0005


def test8():
    assert measures(100, "см", "мм") == 1000


def test9():
    assert measures(100, "см", "дм") == 10


def test10():
    assert measures(100, "см", "км") == 0.001


def test11():
    assert measures(10, "дм", "мм") == 1000


def test12():
    assert measures(10, "дм", "см") == 100


def test13():
    assert measures(10, "дм", "м") == 1


def test14():
    assert measures(10, "дм", "км") == 0.001


def test15():
    assert measures(1, "м", "мм") == 1000


def test16():
    assert measures(1, "м", "см") == 100


def test17():
    assert measures(1, "м", "км") == 0.001


def test18():
    assert measures(3, "км","мм") == 3000000


def test19():
    assert measures(3, "км","см") == 300000


def test20():
    assert measures(3, "км","дм") == 3000


def test21():
    assert measures(9, "йцук","дм") == "Ошибка измерений"